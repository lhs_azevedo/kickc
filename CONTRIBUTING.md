# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
facebook, or any other method with the owner of this repository before making a change. 

## Building KickC

The prerequisites for locally building KickC are the following

*  [Java JDK 8+](https://www.oracle.com/technetwork/java/javase)
*  [Apache Maven 3+](https://maven.apache.org)

The steps to build KickC is

1.  Download, Fork or Clone the source code from https://gitlab.com/camelot/kickc 
2.  Build and package the KickC compiler by entering the source directory and executing `mvn package`


