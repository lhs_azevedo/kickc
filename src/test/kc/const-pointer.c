//Test that constant pointers are detected correctly

void main() {
    byte* screen = $400;
    byte* NULL = $0;
    byte* rem = $ff;

    if(rem!=NULL) {
        screen[0] = '*';
    } else {
        screen[0] = '.';
    }
}