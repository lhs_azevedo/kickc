// Tests the sizeof() operator on values/expressions

byte* const SCREEN = $400;

void main() {
    byte idx = 0;

    // Simple types
    volatile byte b = 0;
    volatile word w = 0;
    // Pointers
    byte* bp = $1000;
    word* wp = &w;

    SCREEN[idx++] = '0'+sizeof(0);
    SCREEN[idx++] = '0'+sizeof(idx);
    SCREEN[idx++] = '0'+sizeof(b);
    SCREEN[idx++] = '0'+sizeof(b*2);
    idx++;
    SCREEN[idx++] = '0'+sizeof($43ff);
    SCREEN[idx++] = '0'+sizeof(w);
    idx++;
    SCREEN[idx++] = '0'+sizeof(bp);
    SCREEN[idx++] = '0'+sizeof(wp);

}