// Tests creating a literal pointer from two bytes

void main() {
    byte* screen = (byte*){ 4, 0 } + (word){ 0, 40 };
    *screen = 'a';
}