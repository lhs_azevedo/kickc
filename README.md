# KickC - Optimizing C-compiler for 6502  

KickC is a C-compiler for creating optimized and readable 6502 assembler code.

The KickC language is classic C with some limitations, some modifications and some extensions to ensure an optimal fit for creating 6502 assembler code. 

* [Download](https://gitlab.com/camelot/kickc/releases) the newest Release 

* [Read](https://docs.google.com/document/d/1JE-Lt5apM-g4tZN3LS4TDbPKYgXuBz294enS9Oc4HXM/edit?usp=sharing) the Reference Manual

* [Look](https://gitlab.com/camelot/kickc/tree/master) through the Source Code

* [Follow](https://gitlab.com/camelot/kickc/issues) the open/closed features being developed

* [Discuss](https://www.facebook.com/groups/302286200587943/) the compiler and receive news on facebook

* [Contribute](https://gitlab.com/camelot/kickc/blob/master/CONTRIBUTING.md) to the development of KickC 

## BETA

KickC is currently in beta, and at times crash or creates ASM code that does not work properly. 
Feel free to test it and report any problems or errors you encounter, but do not expect it to produce production quality code.
Also, be prepared that breaking changes (to syntax, to semantics, etc.) may be implemented in the next versions.